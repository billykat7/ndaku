                                        # NDAKU | Real Estate App |
---------------------------------------------------------------------------------------------------------
                                                WELCOME TEAM

Microservice Architecture - Django, Nodejs, React, Angular, Vue etc ...

GeoLocation Project with Python 3.10, Django 3.2, DRF 3.12, django-countries 7.3 and leaflet 1.0.3
Nodejs 16.16 LTS, React 18 and more info to follow for other technologies

Platform for Buyers, Sellers and Others. Many property types: Commercial, Industrial or Domestic (living).

We will utilize various technologies to accommodate everyone in the team.

---------------------------------------------------------------------------------------------------------
                                        MICROSERVISES - ARCHITECTURE

**DEVOPS**

Tech:

  - [Docker & Docker-compose](https://www.docker.com/)
  - CI/CD with pipeline
  - GitLab
  - API Gateway ([Kong](https://konghq.com/) or [Nginx](https://www.nginx.com/) used as such)
  - [AWS EC2](https://aws.amazon.com/ec2/) - Possibly include [AWS Lambda](https://aws.amazon.com/lambda/)

@alpha_geek -> You'd suggest and we'll decide


**DATABASE**

Tech:

  - [PostgreSQL](https://www.postgresql.org/)
  - [MongoDB](https://www.mongodb.com/)
  - [MySQL](https://www.mysql.com/)
  
@simajacob -> Get some models going and visual.


**Software Developers**

Tech:

  - [Python](https://www.python.org/) ([Django](https://www.djangoproject.com/) / [FastAPI](https://fastapi.tiangolo.com/)) - Backend
  - [GeoDjango - GIS](https://docs.djangoproject.com/en/3.2/ref/contrib/gis/) and [Leaflet](https://leafletjs.com/) for Geo-location
  - [Redis](https://redis.io/), [RabbitMQ](https://www.rabbitmq.com/), 
  - Javascript ([Nodejs](https://nodejs.org/)) - Backend
  - [React](https://reactjs.org/), [Angular](https://angular.io/), [Vue](https://vuejs.org/) 
  - Vanilla Javascript (To accommodate junior Devs)
  - HTML, CSS and [Bootstrap](https://getbootstrap.com/)


**Data Science and Analysis**

To Do:

  - We will have mock data to analyze
  - Machine Learning - Supervised and Unsupervised Learning Models
  - Data Analysis per city, per region and property type.
  - Recommendation engine

Estella will help on this part.


---------------------------------------------------------------------------------------------------------
                                            WORK MODEL

- Tasks will be assigned each team member according to their capabilities and experience
- Sprint is a week long (The period/duration required to complete the task)
- Short Videos on my [YouTube channel](https://www.youtube.com/channel/UCPk2wFJEG6ilxBW70YWDn3Q) to help us on certain instructions or task
- Open to suggestions


---------------------------------------------------------------------------------------------------------
                                        LET'S GO TEAM - YES, WE CAN!

@billykat7   @alpha_geek   @Cre47   @Frank1963-mpoyi   @simajacob   @Nbayondo
